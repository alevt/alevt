%define Name		alevt
%define Version		1.6.3
%define Release		3
%define DocVersion	1.6.3
%define Prefix		/usr
%define ExecPrefix	/usr/X11R6
%define LocaleDir	/usr/share/locale

Summary: Videotext/Teletext
Summary(de): Videotext/Teletext
Name: %{Name}
Version: %{Version}
Release: %{Release}%{LibC}
Copyright: GPL
Group:  X11/Utilities
Source: http://goron.de/~froese/%{Name}/%{Name}-%{Version}.tar.gz
BuildRoot: /tmp/%{Name}-%{Version}-root
URL: http://goron.de/~froese
#Packager: Mario Mikocevic (Mozgy) <mozgy@times.hr>

%changelog
* Mon Jun 14 1999 Karsten Hopp <karsten@delix.de>
- removed old patch from specfile
- removed LibC-Macro
- added '-oldbttv' to the package description.
* Sun May 23 1999 Karsten Hopp <karsten@delix.de>
- several minor patches of Marios spec-file:
  german descriptions
  buildroot (patched Makefile)
  some changed install-paths

%description
Teletext decoder and browser for the bttv driver.

%description -l de
X11 Videotextdecoder f�r den bttv Treiber.

%prep
%setup

%build
make

%install
mkdir -p $RPM_BUILD_ROOT/%{ExecPrefix}/{bin,man/man1}
make rpm-install
gzip -9qnf README CHANGELOG COPYRIGHT $RPM_BUILD_ROOT/%{ExecPrefix}/man/man1/*

%files
%attr(-   ,root,root) %doc README.gz CHANGELOG.gz COPYRIGHT.gz
%attr(0755,root,root) %{ExecPrefix}/bin/alevt
%attr(0755,root,root) %{ExecPrefix}/bin/alevt-date
%attr(0755,root,root) %{ExecPrefix}/bin/alevt-cap
%attr(0644,root,root) %doc %{ExecPrefix}/man/man1/alevt.1x.gz
%attr(0644,root,root) %doc %{ExecPrefix}/man/man1/alevt-date.1.gz
%attr(0644,root,root) %doc %{ExecPrefix}/man/man1/alevt-cap.1.gz
