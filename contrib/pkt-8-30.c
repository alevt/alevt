#/*
gcc pkt-8-30.c -I.. ../vbi.o ../misc.o ../fdset.o ../hamm.o ../lang.o -DVERSION=\"0.0.1\"
exec ./a.out
#*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "vt.h"
#include "fdset.h"
#include "vbi.h"
#include "misc.h"

int debug = 0;

static void
event(void *_, struct vt_event *ev)
{
    switch (ev->type)
    {
	/* vbi may generate EV_PAGE, EV_HEADER, EV_XPACKET */
	/* for event arguments see vt.h */

	case EV_XPACKET: // misc paket
	{
	    if (ev->i1 == 8 && ev->i2 == 30) // broadcast service packet
	    {
		u8 *p = ev->p1;

		printf("designation:     %x\n", p[0]);
		printf("initial page:    %02x\n", p[1]);
		printf("initial subpage: %04x\n", p[5]*256 + p[3]);
		if (p[0]/2 == 0)
		{
		    printf("network code:    %04x\n", p[7]*256 + p[8]);
		    printf("time offset:     %c%d.%d\n",
				(p[9]&0x40)?'-':'+', p[9]/4%16, (p[9]&2)?5:0);
		    printf("mod julian date: %05x\n",
				    p[10]%16*65536+p[11]*256+p[12] - 0x11111);
		    printf("universal time:  %06x\n",
		    			p[13]*65536+p[14]*256+p[15] - 0x111111);
		    printf("info:            %.20s\n", p+20);
		}
		else
		{
		    // programme identification data (VCRs & Co)
		}
		printf("\n");
	    }
	}
    }
}


static void
usage(FILE *fp, int exit_val)
{
    fprintf(fp, "usage: %s [options]\n", prgname);
    fprintf(fp,
	    "\n"
	    "  Valid options:\tDefault:\n"
	    "    --help\n"
	    "    --version\n"
	    "    -vbi <vbidev>\n"
	    );
    exit(exit_val);
}

static int
option(int argc, char **argv, int *ind, char **arg)
{
    static struct { char *nam, *altnam; int arg; } opts[] = {
	{ "-vbi", "-dev", 1 },
	{ "--version", "-v", 0 },
	{ "--help", "-h", 0 },
	{ "-newbttv", "-fuckbttv", 0 },
	{ "-oldbttv", "-old", 0 },
    };
    int i;

    if (*ind >= argc)
	return 0;

    *arg = argv[(*ind)++];
    for (i = 0; i < NELEM(opts); ++i)
	if (streq(*arg, opts[i].nam) || streq(*arg, opts[i].altnam))
	{
	    if (opts[i].arg)
		if (*ind < argc)
		    *arg = argv[(*ind)++];
		else
		    fatal("option %s requires an argument", *arg);
	    return i+1;
	}

    if (**arg == '-')
    {
	fatal("%s: invalid option", *arg);
	usage(stderr, 1);
    }

    return -1;
}

void
main(int argc, char **argv)
{
    int big_buf = -1;
    char *vbi_name = "/dev/vbi";
    struct vbi *vbi;
    int opt, ind;
    char *arg;

    setprgname(argv[0]);

    ind = 1;
    while (opt = option(argc, argv, &ind, &arg))
	switch (opt)
	{
	    case 1:	// -vbi
		vbi_name = arg;
		break;
	    case 2:	// -version
		printf("AleVT-Date Version "VERSION"\n");
		exit(0);
	    case 3:	// help
		usage(stdout, 0);
		break;
	    case 4:	//newbttv
		big_buf = 1;
		break;
	    case 5:	// oldbttv
		big_buf = 0;
		break;
	    case -1:
		usage(stderr, 1);
	}

    fdset_init(fds);

    vbi = vbi_open(vbi_name, 0, 99, big_buf);		// open device
    if (not vbi)
	fatal_ioerror(vbi_name);

    vbi_add_handler(vbi, event, 0);	// register event handler

    for (;;)
	fdset_select(fds, -1);		// call scheduler

    /* never reached */
    vbi_del_handler(vbi, event, 0);
    vbi_close(vbi);
    exit(0);
}
