#/*
gcc t1.c -I.. ../vbi.o ../misc.o ../fdset.o ../hamm.o ../lang.o -DVERSION=\"0.0.1\"
exec ./a.out
#*/

/* crude sample code to read vt-pages */
#include <stdio.h>

#include "vt.h"
#include "fdset.h"
#include "vbi.h"
#include "misc.h"

int debug = 0;

static void
event(int *quitflag, struct vt_event *ev)
{
    switch (ev->type)
    {
	/* vbi may generate EV_PAGE, EV_HEADER, EV_XPACKET */
	/* for event arguments see vt.h */

	case EV_PAGE:	// a complete new page
	{
	    static int npages = 0;
	    struct vt_page *vtp = ev->p1;

	    printf("Got page %x/%x\n", vtp->pgno, vtp->subno);

	    if (++npages >= 16)
		*quitflag = 1;	// quit after 16 pages
	    break;
	}

	case EV_HEADER: // a new title line (for running headers)
	case EV_XPACKET: // an extended packet
	    break;
    }
}


int
main(int argc, char **argv)
{
    int big_buf = -1;		// 0 for -oldbttv, 1 for -newbttv
    struct vbi *vbi;
    int quit = 0;
    
    fdset_init(fds);

    vbi = vbi_open("/dev/vbi", 0, 99, big_buf);	// open device
    vbi_add_handler(vbi, event, &quit);	// register event handler

    // this one isn't really neccessary (in /dev/vbi case!).
    // will always return 0 (nothing in cache yet).
    // Note the hex 0x100!
    vbi_query_page(vbi, 0x100, ANY_SUB);

    while (not quit)
	fdset_select(fds, -1);		// call scheduler

    vbi_del_handler(vbi, event, &quit);
    vbi_close(vbi);
    exit(0);
}
