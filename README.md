# AleVT

This is a program for viewing Videotext/Teletext from /dev/vbi and
/dev/dvb/adapterN/demuxN devices.

This fork of AleVT takes over from where [LinuxTV][legacy] ended. It's
forked from Edgar Törnig's last release in 2003 and incorporates
patches from Debian, LinuxTV and other sources.

 [legacy]: https://www.linuxtv.org/downloads/legacy/

# Use with Digital TV

AleVT does not have any method for changing the channel. One option is
to use `dvbv5-zap` from
[DVBv5 Tools](https://www.linuxtv.org/wiki/index.php/DVBv5_Tools) for this,
another one is to use [VLC](https://www.videolan.org/vlc/). VLC also has a
Teletext browser, so it is a more complete solution.

# Web site

The new web site for AleVT is https://gitlab.com/alevt/alevt
